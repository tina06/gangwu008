package com.github.catvod.spider;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XPathLan extends XPath {

    @Override
    protected void loadRuleExt(String json) {
        super.loadRuleExt(json);
    }
    @Override
    protected String categoryUrl(String tid, String pg, boolean filter, HashMap<String, String> extend) {
        String CateUrl = rule.getCateUrl();
        if (filter && extend != null && extend.size() > 0) {
                for (Iterator<String> it = extend.keySet().iterator(); it.hasNext(); ) {
                    String key = it.next();
                    String value = extend.get(key);
                    if (value.length() > 0) {
                        CateUrl =CateUrl.replace("{" + key + "}", URLEncoder.encode(value));
                    }
                    }
                }
        CateUrl= CateUrl.replace("{cateId}", tid).replace("{catePg}", pg);
        Matcher m = Pattern.compile("\\{(.*?)\\}").matcher(CateUrl);
        while (m.find()) {
            String n=m.group(0).replace("{","").replace("}","");
            CateUrl= CateUrl.replace(m.group(0), "").replace("/"+n+"/","");
        }
        return CateUrl;
    }
}
